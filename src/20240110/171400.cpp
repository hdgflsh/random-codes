// sdsyoj-1834 / luogu-p1972
// [wip]
#include <iostream>
#include <algorithm>
#include <array>
using namespace std;

int n, q, l, r;
array<int, (int)1e6 + 2> a, tree, vis, re;
array<pair<int, pair<int, int>>, (int)1e6 + 2> qu;

struct fiostream
{
    int q;
};

template <typename T>
fiostream &operator>>(fiostream &input, T &r)
{
    char ch;
    r = 0;
    bool s = false;
    while ((ch = getchar()) < '0' || ch > '9')
        s |= ch == '-';
    do
        r = r * 10 + ch - '0';
    while ((ch = getchar()) >= '0' && ch <= '9');
    r *= (s ? -1 : 1);
    return input;
}

void modi(int p, int v)
{
    while (p <= n)
        tree[p] += v, p += p & (-p);
}

int query(int p)
{
    int res = 0;
    while (p)
        res += tree[p], p -= p & (-p);
    return res;
}
int query(int l, int r){
    return query(r) - query(l);
}

int main()
{
    fiostream fin;
    fin >> n;
    for (int i = 1; i <= n; i++)
        fin >> a[i];
    fin >> q;
    for (int i = 0; i < q; i++)
        fin >> l >> r, qu[i] = make_pair(i, make_pair(l, r));
    sort(
        qu.begin(), qu.begin() + q, [](pair<int, pair<int, int>> a, pair<int, pair<int, int>> b)
        { return (a.second.second == b.second.second) ? a.second.first < b.second.first : a.second.second < b.second.second; });
    int powww = 1;
    for (int i = 0; i < q; i++)
    {
        for (int j = powww; j <= qu[i].second.second; j++)
        {
            if (vis[a[j]])
                modi(vis[a[j]], -1);
            modi(j, 1);
            vis[a[j]] = j;
        }
        powww++;
        re[qu[i].first] = query(qu[i].second.first-1, qu[i].second.second);    
    }
    for (int i = 0; i < q; i++)
        cout << re[i] << '\n';
    return 0;
}