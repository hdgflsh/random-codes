// sdsyoj-216 / luogu-p1242
// [wa]
#include <iostream>
#include <array>
#include <vector>
using namespace std;
int n, c, z, r;
array<int, 100> s, d;
void dfs(int x, int y, bool f)
{
    if (s[x] == y)
        return;
    int z = 0;
    while (s[x] == z || y == z)
        z++;
    if (f)
    {
        for (int i = x - 1; i >= 1; i--)
            dfs(i, z, false);
    }
    else
    {
        for (int i = x - 1; i >= 1; i--)
            dfs(i, y, false);
        int sx = s[x];
        dfs(x, z, false);
        for (int i = x - 1; i >= 1; i--)
            dfs(i, sx, false);
    }
    cout << "move " << x << " from " << char(s[x] + 'A') << " to " << (char)(y + 'A') << '\n';
    s[x] = y;
    r++;
    return;
}
int main()
{
    cin >> n;
    for (int i = 0; i < 6; i++)
    {
        cin >> c;
        while (c--)
            cin >> z, (i < 3) ? s[z] = i : d[z] = i - 3;
    }
    for (int i = n; i >= 1; i--)
        dfs(i, d[i], true);
    cout << r;
    return 0;
}