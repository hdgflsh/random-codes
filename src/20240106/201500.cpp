// atcoder-abc335-c
// [ac]
#include <iostream>
#include <array>
using namespace std;
int t, n, offs, q;
char d;
array<pair<int, int>, (int)1e6 + 2> p;

pair<int, int> operator+(pair<int, int> pii, char c)
{
    if (c == 'U')
        return make_pair(pii.first, pii.second + 1);
    else if (c == 'D')
        return make_pair(pii.first, pii.second - 1);
    else if (c == 'L')
        return make_pair(pii.first - 1, pii.second);
    else
        return make_pair(pii.first + 1, pii.second);
}

int main()
{
    cin >> n >> q;
    for (int i = 0; i < n; i++)
        p[i] = make_pair(i + 1, 0);
    offs = n - 1;
    while (q--)
    {
        cin >> t;
        if (t == 1)
        {
            cin >> d;
            p[offs] = p[(offs + 1) % n] + d;
            (offs == 0) ? offs = n - 1 : offs--;
        }
        else
        {
            cin >> t;
            cout << p[(t + offs) % n].first << ' ' << p[(t + offs) % n].second << '\n';
        }
    }
    return 0;
}