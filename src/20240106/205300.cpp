// atcoder-abc335-d
#include <iostream>
using namespace std;
long long n, l;
int main()
{
    cin >> n;
    for (int y = 1; y <= n; y++)
        for (int x = 1; x <= n; x++)
            if ((x == (n + 1) / 2) && (y == (n + 1) / 2))
                cout << 'T' << ((x == n) ? '\n' : ' ');
            else
                cout << ((y < (n - x + 1)) ? (l = (n + 1) / 2 - y, n * n - 4 * l * (l + 1) + (x - y)) : (((n - x + 1) < (n - y + 1)) ? (l = (n + 1) / 2 - (n - x + 1), n * n - 4 * l * (l + 1) + 2 * l + (y - (n - x + 1))) : (((n - y + 1) < x) ? (l = (n + 1) / 2 - (n - y + 1), n * n - 4 * l * (l + 1) + 4 * l + (y - x)) : (l = (n + 1) / 2 - x, n * n - 4 * l * (l + 1) + 6 * l + (n - y + 1 - x))))) << ((x == n) ? '\n' : ' ');
    return 0;
}
/*
1. layer?
    n/2 - min(x, n-x+1, y, n-y+1)
3. count of num?
    layer n(count from inner) has 8n numbers, each side 2n.
    total 2^n-1 numbers, n/2-1 layers.
    layer 1 to n-1 have 4n*(n-1) numbers.
    layer n have 8n numbers, each side 2n.
    layer n have numbers from 2^n - 4n(n+1) to 2^n - 1 - 4n(n-1).
2. side?
    (y < (n-x+1)) ?  // y = top
        l = n/2-y, 2^n - 4*l*(l+1) + (x - y);
     : ((n-x+1) < (n-y+1)) ? //  -> right
        l = n/2-(n-x+1), 2^n - 4 * l * (l+1) + 2*l + (y - (n - x + 1));
     : ((n-y+1) < x) ? // -> bottom
        l = n/2-(n-y+1), 2^n - 4*l*(l+1) + 4*l + ((n - x + 1) - (n - y + 1));
     : //x -> left,
        l = n/2-x, 2^n - 4*l*(l+1) + 6*l + ((n - y + 1) - x);


8 + 2*8 + 3*8 + ... + (n-1)*8
4n * (n-1)
24 - 8

3 2 3 4
1 2 3 4 5
*/