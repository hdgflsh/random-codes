// luogu-p1429
// [wa]
#include <iostream>
#include <iomanip>
#include <array>
#include <algorithm>
#include <cmath>
using namespace std;
int n, ra, rb;
double d_min = 1e20;
array<pair<int, int>, (int)2e5 + 3> p;

istream &operator>>(istream &is, pair<int, int> &p)
{
    int x, y;
    is >> x >> y;
    p = make_pair(x, y);
    return is;
}

double operator-(const pair<int, int> y, const pair<int, int> z)
{
    return sqrt(pow(y.first - z.first, 2) + pow(y.second - z.second, 2));
}
auto cmp_y = [](pair<int, int> a, pair<int, int> b)
{ return (a.second == b.second) ? a.first < b.first : a.second < b.second; };

void dc(int l, int r)
{
    if (r - l <= 4)
    {
        for (int i = l; i < r; i++)
            for (int j = i + 1; j < r; j++)
                if (p[i] - p[j] < d_min)
                    d_min = p[i] - p[j], ra = i, rb = j;
        sort(p.begin() + l, p.begin() + r, cmp_y);
        return;
    }
    int m = (l + r) / 2;
    dc(l, m);
    dc(m, r);
    inplace_merge(p.begin() + l, p.begin() + m, p.begin() + r, cmp_y);
    int tsz = 0;
    for (int i = l; i < r; i++)
    {
        if (abs(p[i].first - p[m].first) >= d_min)
            continue;
        for (int j = tsz - 1; j >= 0 && p[i].second - p[j].second < d_min; j--)
            if (p[i] - p[j] < d_min)
                d_min = p[i] - p[j], ra = i, rb = j;
        tsz++;
    }
}
int main()
{
    cin >> n;
    for (int i = 0; i < n; i++)
        cin >> p[i];
    sort(p.begin(), p.begin() + n);
    dc(0, n);
    cout << fixed << setprecision(4) << d_min;
    return 0;
}