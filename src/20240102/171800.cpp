// sdsyoj-279
// [ac]
#include <iostream>
#include <array>
#include <cmath>
#include <algorithm>
using namespace std;
int n, r, ma;
array<int, 1000006> a, z, maxa;
int main()
{
    cin >> n;
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    for (int i = n; i >= 1; i--)
        maxa[i] = ma = max(ma, a[i]);
    int j;
    for (int i = 1; i <= n; i++)
    {
        while (maxa[i] < z[r])
            cout << z[r--] << ' ';
        j = i;
        while (a[j] != maxa[i])
            z[++r] = a[j], j++;
        cout << maxa[i] << ' ';
        i = j;
    }
    for (int i = r; i >= 1; i--)
        cout << z[i] << ' ';
    return 0;
}